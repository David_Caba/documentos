

require('./bootstrap');
/*
window.Vue = require('vue').default;

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
*/

import Vue from 'vue'
import Main from './Main'
import router from './routes/routes'
import store from './store'


store.dispatch('auth/mex').then(()=>{
    new Vue({
        router,
        store,
        render:h => h(Main) 
    }).$mount('#app')
})