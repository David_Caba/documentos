import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home'
import SignIn from '../pages/SignIn'
import SignUp from '../pages/SignUp'
import Dashboard from '../pages/Dashboard'
import Registro from '../pages/Registro'
import Estadistica from '../pages/Estadistica'
import Sidebar from '../components/Sidebar'
import Nav from '../components/Nav'
import Cuadros from '../components/Cuadros'
import Inicios from '../components/Inicios'
import Usuarios from '../pages/Usuarios'
import User_profile from '../pages/User_profile'
import Maps from '../pages/Maps'



Vue.use(VueRouter)
const routes=[   
    {path:"/",name:'home',component:Home},
    {path:"/signin",name:'signin',component:SignIn},
    {path:"/signup",name:'signup',component:SignUp},
    {path:'/dashboard',name:'dashboard',component:Dashboard},
    {path:'/registro',name:'registro',component:Registro},
    {path:'/estadistica',name:'estadistica',component:Estadistica},
    {path:'/sidebar',name:'sidebar',component:Sidebar},
    {path:'/nav',name:'nav',component:Nav},
    {path:'/cuadros',name:'cuadros',component:Cuadros},
    {path:'/inicios',name:'inicios',component:Inicios},
    {path:'/usuarios',name:'usuarios',component:Usuarios},
    {path:'/user_profile',name:'user_profile',component:User_profile},
    {path:'/maps',name:'maps',component:Maps},



   
]

const router=new VueRouter({
    mode:'history',
    base:process.env.BASE_URL,
    routes
})

export default router