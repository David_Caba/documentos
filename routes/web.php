<?php

use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('init');

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/{any}',[App\Http\Controllers\HomeController::class, 'index'])->where('any','.*');

//Route::get('/pdf','PDFController@PDFUser')->name('descargarPDF');
//Route::get('/listar_usuarios',[App\Http\Controllers\HomeController::class, 'listar'])->name('listar_usuarios');
