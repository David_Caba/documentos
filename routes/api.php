<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('/bd_register_box',App\Http\Controllers\RegisterController::class);               

Route::get('/listar_usuarios', [App\Http\Controllers\HomeController::class, 'listar'])->name('listar_usuarios');
Route::post('/crear_usuarios', [App\Http\Controllers\HomeController::class, 'store'])->name('crear_usuarios');
Route::put('/editar_usuarios/{idUser}', [App\Http\Controllers\HomeController::class, 'update'])->name('editar_usuarios');
Route::delete('/eliminar_usuarios/{idUser}', [App\Http\Controllers\HomeController::class, 'delete'])->name('eliminar_usuarios');


Route::delete('/eliminar/{idUser}', [App\Http\Controllers\prueba::class, 'delete'])->name('eliminar');
Route::put('/editar/{idUser}', [App\Http\Controllers\prueba::class, 'update'])->name('editar');

Route::post('/save_Photo', [App\Http\Controllers\prueba::class, 'savePhoto'])->name('save_Photo');

