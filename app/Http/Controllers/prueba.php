<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Register;
use Illuminate\Support\Str;

class prueba extends Controller
{
    public function delete(Register $idUser){
        $idUser->delete();
    }

    public function update(Request $request,Register $idUser){
        $data = request()->validate([
            'contratista' => ['string', 'max:255'],
            'descripcion' => ['string', 'max:255'.$idUser->id],
            'file' => ['string'],
            'codigo' => ['integer', 'min:4', 'unique:registers,codigo'],
        ]);
        $idUser->update($data);
    }
    public function savePhoto(Request $request)
    {
        
        if ($request->hasFile('profile_pic'))
      {


            $file      = $request->file('profile_pic');
            $filename  = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            
            $picture   = date('His').'-'.$filename;
            $file->move(public_path('img'), $picture);

            $url=url($filename);
            
            Register::create([
                
                'file'=>$url
            ]);
            return response()->json(["message" => "Image Uploaded Succesfully"]);
             
        } 
      else
      {
            return response()->json(["message" => "Select image first."]);
      }

              

    }

}
