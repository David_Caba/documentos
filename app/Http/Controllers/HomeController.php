<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        return view('welcome');
    }
    public function listar()
    {
        return User::All();
    }
    public function store(Request $request){
        $data = request()->validate([
            'name' => ['string', 'max:255'],
            'email' => ['string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5'],
        ]);
        $data['password'] = Hash::make($data['password']);
        User::create($data);
    }

    public function update(Request $request, User $idUser){
        $data = request()->validate([
            'name' => ['string', 'max:255'],
            'email' => ['string', 'email', 'max:255', 'unique:users,email,'.$idUser->id],
            'password' => ['nullable', 'string', 'min:5'],
        ]);
        // validar si la contraseña es vacia
        if (empty($request->password)){
            $data['password'] = $idUser->password;
        }else{
            $data['password'] = Hash::make($data['password']);
        }
            $idUser->update($data);
        }
        
    public function delete(User $idUser){
        $idUser->delete();
    }


}
