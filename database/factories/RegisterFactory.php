<?php

namespace Database\Factories;

use App\Models\Register;
use Illuminate\Database\Eloquent\Factories\Factory;

class RegisterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Register::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        
            'contratista' => $this->faker->name,
            //'especialidad' => $this->faker->sentence(rand(1,5)),
            'descripcion' => $this->faker->paragraph(5),
            //'archivo' => $this->faker->sentence(rand(5,10)),

            //'pertenece' => $this->faker->sentence(rand(1,3)),
            'codigo' => $this->faker->numberBetween(0,300),
 
        ];

        
        
       
    }
}
